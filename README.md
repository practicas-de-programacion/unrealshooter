# Risketos Basicos:

- [X] Control de personaje con base ACharacter
- [X] Capacidad de disparo
- [X] Delegados para alguna accion (disparo)
- [X] Uso de UMacros
- [X] Multiples armas

# Risketos Opcionales:

- [X] IA de los enemigos BehaviorTree
- [X] BlendTree
- [X] Soltar las armas

# Controles:
- WASD - moverse
- Spacio - saltar
- Click izquierdo - disparar
