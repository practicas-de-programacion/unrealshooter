
#pragma once

#include "CoreMinimal.h"
#include "BPC_Weapon.h"
#include "BPC_Copeta.generated.h"

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER24IKERRAMIREZ_API UBPC_Copeta : public UBPC_Weapon
{
	GENERATED_BODY()
public:
	
	UBPC_Copeta();

	void OnFireCallback() override;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float mSpread{0.2f};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float mPellets{20};
};
