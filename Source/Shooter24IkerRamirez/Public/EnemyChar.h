// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EnemyChar.generated.h"

UCLASS()
class SHOOTER24IKERRAMIREZ_API AEnemyChar : public ACharacter
{
	GENERATED_BODY()

public:
	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
	void OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType, AController* apInstigatedBy, AActor* apDamageCauser);

	
	AEnemyChar();

protected:
	virtual void BeginPlay() override;

};
