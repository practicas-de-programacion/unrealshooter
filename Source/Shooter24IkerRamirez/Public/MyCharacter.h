
#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "MyCharacter.generated.h"

class UInputMappingContext;
class UInputConfigData;
class UBPC_Weapon;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnClick);

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDropWaepon);

UCLASS()
class SHOOTER24IKERRAMIREZ_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMyCharacter();

	void Tick(float DeltaTime) override;
	
	void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UStaticMeshComponent* GetWaepon() const {return mWeaponPoint;}
	
	UCameraComponent* GetCamera() const {return mpCamera;}

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* mWeaponPoint;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputMappingContext* mInputMapping;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		UInputConfigData* mInputData;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int mMoveSpeed {2U};

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mMaxHealth {100.f};
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		float mCurrentHealth {100.f};

	UPROPERTY(BlueprintAssignable)
		FOnClick evOnClick;

	UPROPERTY(BlueprintAssignable)
		FOnDropWaepon evOnDrop;
	
		UBPC_Weapon* mpWaeponEquipped;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:

	void MoveCallback(const FInputActionValue&	aValue);

	void LookCallback(const FInputActionValue&	aValue);

	void OnClickCallBack(const FInputActionValue&	aValue);

	void OnDropCallBack(const FInputActionValue& aValue);
	
	UPROPERTY(EditDefaultsOnly)
		UCameraComponent* mpCamera;
	
};
