
#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cubo.generated.h"

UCLASS()
class SHOOTER24IKERRAMIREZ_API ACubo : public AActor
{
	GENERATED_BODY()
	
public:
	UFUNCTION()
	void OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType, AController* apInstigatedBy, AActor* apDamageCauser);

	ACubo();
	
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		UStaticMeshComponent* mpMesh;
	
 	
protected:
	virtual void BeginPlay() override;

};
