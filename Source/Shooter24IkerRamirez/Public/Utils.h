#pragma once

#define screenD(x) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT(x));}
#define screenDv(x) if(GEngine) { GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, x);}

#define LogD(x) UE_LOG(LogTemp, Warning, TEXT(x));

#define Format1(x,y) FString::Printf(TEXT(x),y)