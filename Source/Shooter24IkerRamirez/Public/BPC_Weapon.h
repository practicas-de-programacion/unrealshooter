
#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "MyCharacter.h"
#include "Components/BillboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Shooter24IkerRamirez/Public/Utils.h"


#include "BPC_Weapon.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SHOOTER24IKERRAMIREZ_API UBPC_Weapon : public UActorComponent
{
	GENERATED_BODY()

public:	

	UBPC_Weapon();
	
	void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void AttatchWaepon(AMyCharacter* apPlayerCharacter);

	UFUNCTION(BlueprintCallable)
		virtual void OnFireCallback();

	UFUNCTION(BlueprintCallable)
		void DropWaepon();
	
	UPROPERTY(EditDefaultsOnly)
		UBillboardComponent* mpMuzzleOffset;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mShootRange{5000.0f};
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		float mDamage{2.0f};

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		AMyCharacter* mpOwnerCharacter;
	
};
