

#include "Cubo.h"

ACubo::ACubo()
{
 	PrimaryActorTick.bCanEverTick = true;

	mpMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Cubo"));
	mpMesh->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<UStaticMesh> defaultCube(TEXT("/Engine/BasicShapes/Cube.cube"));
	if(defaultCube.Succeeded())
	{
		mpMesh->SetStaticMesh(defaultCube.Object);
	}

	mpMesh->BodyInstance.SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	
	OnTakeAnyDamage.AddDynamic(this, &ACubo::OnHitCallback);
}	

void ACubo::BeginPlay()
{
	Super::BeginPlay();

}

void ACubo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACubo::OnHitCallback(AActor* apDamagedActor, float aDamage, const UDamageType* apDamageType,
	AController* apInstigatedBy, AActor* apDamageCauser)
{
	Destroy();
}