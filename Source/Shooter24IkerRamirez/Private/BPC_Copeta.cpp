
#include "BPC_Copeta.h"

UBPC_Copeta::UBPC_Copeta()
{
	mShootRange = 2000.0;
}

void UBPC_Copeta::OnFireCallback()
{
    if(mpOwnerCharacter == nullptr) return;
    const UWorld* pWorld {GetWorld()};
    if(pWorld == nullptr) return;

    UCameraComponent* PlayerCamera {mpOwnerCharacter-> GetCamera()};
    const FRotator CameraRotator{PlayerCamera->GetComponentRotation()};
    const FVector StartLocation{GetOwner()->GetActorLocation() + CameraRotator.RotateVector(mpMuzzleOffset->GetComponentLocation())};
   
    FCollisionQueryParams QueryParams{};
    QueryParams.AddIgnoredActor(mpOwnerCharacter);

    for (int i = 0; i< mPellets;i++)
    {

    	FRotator deviation;
    	deviation.Yaw = FMath::RandRange(-mSpread,mSpread) * 90.f;
    	deviation.Pitch =  FMath::RandRange(-mSpread,mSpread) * 90.f;
    	
    	FVector EndLocation{StartLocation + UKismetMathLibrary::GetForwardVector(PlayerCamera->GetComponentRotation() +  deviation) * mShootRange};
    	
    	FHitResult HitResult{};
    	pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
    	DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Yellow : FColor::Purple, false, 0.5f, 0, 10.0f);

    	if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
    	{
    		screenDv(Format1("%s", *HitResult.GetActor()->GetName()));
    		UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage, mpOwnerCharacter->GetController(), GetOwner(),{});
    	}
    }
    
    
	
}
