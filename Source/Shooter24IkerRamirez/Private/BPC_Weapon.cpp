
#include "BPC_Weapon.h"



UBPC_Weapon::UBPC_Weapon()
{
	PrimaryComponentTick.bCanEverTick = true;

	mpMuzzleOffset = CreateDefaultSubobject<UBillboardComponent>(TEXT("ShootPoint"));
}

void UBPC_Weapon::AttatchWaepon(AMyCharacter* apPlayerCharacter)
{
	mpOwnerCharacter = apPlayerCharacter;

	if(mpOwnerCharacter != nullptr && mpOwnerCharacter->mpWaeponEquipped == nullptr)
	{
		mpOwnerCharacter->mpWaeponEquipped = this;
		FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
		GetOwner()->AttachToComponent(mpOwnerCharacter->GetWaepon(), AttachRules, TEXT("WaeponPointSocket"));

		mpOwnerCharacter->evOnClick.AddDynamic(this, &UBPC_Weapon::OnFireCallback);
		mpOwnerCharacter->evOnDrop.AddDynamic(this, &UBPC_Weapon::DropWaepon);
	}
}

void UBPC_Weapon::OnFireCallback()
{
	if(mpOwnerCharacter == nullptr) return;
	const UWorld* pWorld {GetWorld()};
	if(pWorld == nullptr) return;

	UCameraComponent* PlayerCamera {mpOwnerCharacter-> GetCamera()};
	const FRotator CameraRotator{PlayerCamera->GetComponentRotation()};
	const FVector StartLocation{GetOwner()->GetActorLocation() + CameraRotator.RotateVector(mpMuzzleOffset->GetComponentLocation())};
	const FVector EndLocation{StartLocation + UKismetMathLibrary::GetForwardVector(PlayerCamera->GetComponentRotation())*mShootRange};

	FCollisionQueryParams QueryParams{};
	QueryParams.AddIgnoredActor(mpOwnerCharacter);

	FHitResult HitResult{};
	pWorld->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Camera, QueryParams);
	DrawDebugLine(pWorld, StartLocation, EndLocation, HitResult.bBlockingHit ? FColor::Blue : FColor::Red, false, 0.5f, 0, 10.0f);

	if(HitResult.bBlockingHit && IsValid(HitResult.GetActor()))
	{
		screenDv(Format1("%s", *HitResult.GetActor()->GetName()));
		UGameplayStatics::ApplyDamage(HitResult.GetActor(), mDamage, mpOwnerCharacter->GetController(), GetOwner(),{});
	}
	
}

void UBPC_Weapon::DropWaepon()
{
	
	if(mpOwnerCharacter != nullptr && mpOwnerCharacter->mpWaeponEquipped != nullptr)
	{

		mpOwnerCharacter->evOnDrop.RemoveDynamic(this, &UBPC_Weapon::OnFireCallback);
		mpOwnerCharacter->evOnDrop.RemoveDynamic(this, &UBPC_Weapon::DropWaepon);

		FDetachmentTransformRules DettachRules {EDetachmentRule::KeepWorld, false};
		GetOwner()->DetachFromActor(DettachRules);
		
		mpOwnerCharacter->mpWaeponEquipped = nullptr;
		
		mpOwnerCharacter = nullptr;
	}
	
}


void UBPC_Weapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

