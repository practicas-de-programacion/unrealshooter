// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputConfigData.h"
#include "Utils.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Shooter24IkerRamirez/Public/Utils.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	mWeaponPoint = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WaeponPoint"));
	mWeaponPoint -> bCastDynamicShadow = false;
	mWeaponPoint -> CastShadow = false;

	mpCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("MainCamera"));
	mpCamera-> SetupAttachment(GetMesh());
	mpCamera->bUsePawnControlRotation = true;

	UCharacterMovementComponent* mMovimiento{GetCharacterMovement()};
	mMovimiento->BrakingFriction = 10.0f;
	mMovimiento->MaxAcceleration = 10000.f;
	mMovimiento->MaxWalkSpeed = 1000.0f;
	mMovimiento->JumpZVelocity = 500.0f;
	mMovimiento->AirControl = 0.8f;
}

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();
	mWeaponPoint->AttachToComponent(GetMesh(), {EAttachmentRule::SnapToTarget, true}, TEXT("WaeponPointSocket"));
	
	FAttachmentTransformRules AttachRules {EAttachmentRule::SnapToTarget, true};
	mpCamera->AttachToComponent(GetMesh(), AttachRules, TEXT("HeadPointSocket"));

	
}

// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	APlayerController* PlayerController = Cast<APlayerController>(GetController());

	UEnhancedInputLocalPlayerSubsystem* EInputSubsistem = ULocalPlayer :: GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer());
	EInputSubsistem->ClearAllMappings();
	EInputSubsistem->AddMappingContext(mInputMapping, 0);

	UEnhancedInputComponent* EInputComponent {Cast<UEnhancedInputComponent>(PlayerInputComponent)};
	EInputComponent->BindAction(mInputData->InputMove, ETriggerEvent::Triggered, this, &AMyCharacter::MoveCallback);
	EInputComponent->BindAction(mInputData->InputLook, ETriggerEvent::Triggered, this, &AMyCharacter::LookCallback);
	EInputComponent->BindAction(mInputData->InputJump, ETriggerEvent::Triggered, this, &AMyCharacter::Jump);
	EInputComponent->BindAction(mInputData->InputMouseClick, ETriggerEvent::Started, this, &AMyCharacter::OnClickCallBack);
	EInputComponent->BindAction(mInputData->InputDrop, ETriggerEvent::Started, this, &AMyCharacter::OnDropCallBack);

	
}


void AMyCharacter::MoveCallback(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2D MoveValue {aValue.Get<FVector2D>()};
		const FRotator MoveRotator {0, Controller->GetControlRotation().Yaw, 0};

		if(MoveValue.Y != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::ForwardVector)};
			AddMovementInput(Dir, MoveValue.Y);
		}
		if(MoveValue.X != 0.0f)
		{
			const FVector Dir {MoveRotator.RotateVector(FVector::RightVector)};
			AddMovementInput(Dir, MoveValue.X);
		}
		
	}
}

void AMyCharacter::LookCallback(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		const FVector2D LookValue{aValue.Get<FVector2D>()};


		if(LookValue.X != 0)
		{
			AddControllerYawInput(LookValue.X);
		}

		if(LookValue.Y != 0)
		{
			AddControllerPitchInput(LookValue.Y);
		}
		
	}

	
}

void AMyCharacter::OnClickCallBack(const FInputActionValue& aValue)
{
	if(IsValid(Controller))
	{
		evOnClick.Broadcast();

	}
}

void AMyCharacter::OnDropCallBack(const FInputActionValue& aValue)
{

	if(IsValid(Controller))
	{
		evOnDrop.Broadcast();
	}
	
}



