// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Shooter24IkerRamirezGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SHOOTER24IKERRAMIREZ_API AShooter24IkerRamirezGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

};
